destinationDir=/opt/LynxCut
THIS_FILE := $(lastword $(MAKEFILE_LIST))

all: install

install: 
	@echo -n "Installing LynxCut .. " ||:
	@sudo mkdir -p $(destinationDir)/LynxCut ||:
	@sudo cp -r . $(destinationDir) ||:
	@sudo ln -sf $(destinationDir)/LynxCut /usr/local/bin/lynxcut ||:
	@sudo ln -sf $(destinationDir)/UPDATE /usr/local/bin/lynxcut-ud ||:
	@sudo ln -sf $(destinationDir)/UNINSTALL /usr/local/bin/xx-ui ||:
	@sudo chmod +x $(destinationDir)/LynxCut ||:
	@sudo chmod +x $(destinationDir)/UPDATE ||:
	@sudo chmod +x $(destinationDir)/UNINSTALL ||:
	@echo "Done" ||:
	@echo
	@echo "Installation summary" ||:		
	@echo '╔══════════════╦════════════════════════════════════════════════════╗' ||:
	@echo '║   Command    ║                       usage                        ║' ||:
	@echo '╠══════════════╬════════════════════════════════════════════════════╣' ||:
	@echo '║ lynxcut      ║ LynxC application                                   ║' ||:
	@echo '║ lynxcut-ud    ║ Updates LynxC to latest version from the repository ║' ||:
	@echo '║ xx-i ║ Uninstalls LC completely                         ║' ||:
	@echo '╚══════════════╩════════════════════════════════════════════════════╝' ||:

uninstall:
	@echo -n "Uninstalling LynxCut .. " ||:
	@sudo rm $(destinationDir)/* -rf ||:
	@sudo rm /usr/local/bin/lynxcut ||:
	@sudo rm /usr/local/bin/lynxcut-ud ||:
	@sudo rm /usr/local/bin/lynxcut-ui ||:
	@echo "Done" ||:

update:
	@echo "Updating xcut:" ||:
	@echo -n "    " ||:
	@$(MAKE) -f $(THIS_FILE) uninstall --no-print-directory
	@echo -n "    " ||:
	@$(MAKE) -f $(THIS_FILE) install --no-print-directory
	@echo "Done" ||:
